FROM UBUNTU
RUN apt install node js 
COPY ./web /web/
WORKDIR /web/
EXPOSE 80
CMD ["npm" ,"run" ,"start"]